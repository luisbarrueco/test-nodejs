# Use Alpine Linux
FROM alpine:edge

# Declare maintainer
MAINTAINER Luis Barrueco <luis.barrueco@gmail.com>

# Set up NodeJs
RUN apk add --update nodejs=6.7.0-r0 && \
    mkdir /tmp/src /src && \
    rm -rf /var/cache/apk/*

# Expose ports
EXPOSE 8080

COPY . /opt/app
WORKDIR /opt/app

# Entry point
CMD ["npm", "start"]
